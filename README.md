# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* OSBDealerCheckScript
* Current Version 2

### How do I get set up? ###

Setup

* Get a copy of the workbook OSBDealerDataCheck.xlsm
* open workbook
* enable VBA

VBA project needs the following references:

* Visual Basic for Applications
* Microsoft Excel 15.0 Object Library
* OLE Automation
* Microsoft XML v6.0
* Microsoft Scripting Runtime
* Microsoft Forms 2.0 Object Library

(To view references, open VBA Editor, Tools -> References)

Mulesoft accounts

* CLIENT_ID and CLIENT_SECRET need to be valid and active


### Contribution guidelines ###

* VBA project is password locked. See OSBDealerCheckScript.txt for password

### Who do I talk to? ###

* Check script written by Michael Ruschena.
